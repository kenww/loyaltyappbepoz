angular.module('starter.controllers')

.controller('PageCtrl', function($scope, $rootScope, $stateParams, $timeout, $state, $ionicLoading, Listings, OrderModal, enquiryPopup){
  console.log($stateParams);

  $scope.btnActived = function(page){
    if($scope.btnActive == page){
      return true;
    }else {
      return false;
    }
  };

  $scope.changeBtn = function(page){
    $scope.btnActive = page;
    // resizeImgs();
  }

  $scope.init = function(){
    $scope.pages.forEach(function(page){
      Listings.getListings(page.listing_type_id).then(function(s){
        page.listings = $rootScope.objToArr(s);
        $ionicLoading.hide();
      }, function(e){
        $ionicLoading.hide();
        $scope.listings = [];
      })
    });

    $timeout(function(){
      $scope.$broadcast('scroll.refreshComplete');
      console.log($scope.pages);
      // resizeImgs();
    }, 1000)
  };

  function resizeImgs(){
    $timeout(function(){
      var listingImgElements = angular.element(document.getElementsByClassName('listing-banner-img'));
      var ratio = 120/375;
      console.log(listingImgElements);
      listingImgElements.css('height', listingImgElements[0].clientWidth*ratio);
    });
  }

  // $scope.init();
  if(typeof $rootScope['layout'] != 'undefined'){
    $scope.pages = $rootScope.layout[$stateParams.view.layout][$stateParams.view.index].pages;
    $scope.pageName = $rootScope.layout[$stateParams.view.layout][$stateParams.view.index].page_name;

    $scope.btnActive = $scope.pages[0];
    $scope.init();
  }

  $rootScope.$on('LayoutReady', function(event, data){
    $scope.pages = $rootScope.layout[$stateParams.view.layout][$stateParams.view.index].pages;
    $scope.pageName = $rootScope.layout[$stateParams.view.layout][$stateParams.view.index].page_name;
    $scope.btnActive = $scope.pages[0];
    $scope.init();
  });

  $scope.shortDay = function(day){
    return day.name.substring(0,3);
  }

  $scope.openItem = function(listingID){
    $state.go("menu.listing", {id:listingID});
  }


  $scope.btnRows = function(col){
        if($scope.btnActive.listings.length <= 0){
            return 0;
        }
        return Math.ceil($scope.btnActive.listings.length/col);
  };

  $scope.bookGC = function(listing, flag){
    OrderModal.showGCOrder(listing, flag);
  }

  $scope.enquiry = function(listing){
    enquiryPopup.show(listing, $scope).then(function(r){
      console.log(r);
      //submit enquiry here
    });
  }

  //
  // accordion
  //
  $scope.openedItem = null;
  $scope.openItemInpage = function(listing){
    if($scope.openedItem === listing){
      $scope.openedItem = null;
    }
    else {
      $scope.openedItem = listing
    }
  }

  $scope.isOpenItemInpage = function(listing){
    if(listing === $scope.openedItem){
      return true;
    }
    return false;
  }

  $timeout(function(){
    console.log($stateParams.view.view);
    window.ga.trackView($stateParams.view.view,'', true);
  });


});
